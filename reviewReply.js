const cheerio = require('cheerio');
const puppeteer = require('puppeteer')

// const mysql = require('mysql');


// const db = mysql.createConnection({
//     database: 'sololuxu_amourint_erp',
//     username: 'root',
//     password: '',
//     user: 'root'
// })



// let Queries = [{
//     url: 'nordstrom.com',
//     name: 'Nordstrom'
// }]




// function Comment(website, brand, review_url, username, title, body, stars) {
//     this.website = website;
//     this.brand = brand;
//     this.review_url = review_url;
//     this.username = username;
//     this.title = title;
//     this.body = body;
//     this.stars = stars;
//     this.created_at = new Date().toISOString().slice(0, 19).replace('T', ' ');
//     this.updated_at = new Date().toISOString().slice(0, 19).replace('T', ' ');
// }


// db.connect(async err => {
//     if (err) console.log(err);
//     else {
//         console.log('sql connected')
//         await SITEJABBER.beginScrapping().then(res => {
//             db.end();
//         });
//     };

// })


reviewReply = {
    query: '',
    stop: 0,
    id: 0,
    commentFound: 0,
    pageNumber: 1,
    scrapedComment: '',
    query: '',
    firstOne(query) {
        console.log(query)
        return new Promise(async resolve => {
            let URL = 'https://biz.sitejabber.com/login';
            console.log(URL);
            const browser = await puppeteer.launch({
                args: ['--no-sandbox', '--disable-setuid-sandbox']
            });
            const page = await browser.newPage();
            await page.setViewport({
                width: 1920,
                height: 1080
            });
            await page.goto(URL, {
                waitUntil: 'networkidle2'
            });
            await page.waitFor(2000);
            await page.type("#email", 'info@sololuxury.co.in')
            await page.type('#password', 'Solo123!@#')
            await page.click("button[type='submit']");
            await page.waitFor(5000);
            this.pageNumber = 1;
            while (this.stop != 1) {
                await page.goto('https://biz.sitejabber.com/reviews?page=' + this.pageNumber, {
                    waitUntil: 'networkidle2'
                })
                await page.waitFor(5000);
                let divs = await page.$$(".media-body");
                if (divs.length > 0) {
                    for (let i = 0; i < divs.length; i++) {
                        if (this.commentFound != 1 && this.stop != 1) {
                            let html = await page.evaluate(e => e.innerHTML, divs[i])
                            await this.getTitle(html).then(resolve => {
                                if (resolve != '0') {
                                    this.scrapedComment = resolve;
                                    console.log(this.scrapedComment);
                                }
                            });
                            if (this.scrapedComment.includes(query.comment)) {
                                console.log("found the comment");
                                this.id = this.id.replace("comment-id-", '')
                                console.log(this.id);
                                this.stop = 1;
                                this.commentFound = 1;
                                await this.postComment(page, i, query)
                            }
                        }
                    }
                } else {
                    this.stop = 1;
                }
                this.pageNumber++;
                console.log("page =" + this.pageNumber)
            }
            browser.close();
            resolve('done')
        })
    },

    postComment(page, i, query) {
        return new Promise(async resolve => {
            if (this.pageNumber > 1) i = i - 1;
            let cmtHolders = await page.$$('.comment_hidden');
            let cmt = await cmtHolders[i];
            await cmt.click()
            await page.waitFor(5000);
            try {
                await page.type('#ReviewCommentContent-' + this.id, query.reply, {
                    delay: 20
                })
                await page.waitFor(4000)
                let btn = await page.$('#btn-' + this.id);
                await btn.click()
                await page.waitFor(13000);
            } catch (err) {
                console.log("already replied")
            }
            resolve('done')
        })
    },

    getTitle(html) {
        return new Promise(resolve => {
            const $ = cheerio.load(html);
            try {
                let holder = $('.media-heading');
                let review = holder[0].children[0].data;
                holder = $('.control-label');
                this.id = holder[0].attribs.for;
                resolve(review)
            } catch (err) {
                resolve('0');
            }

        })
    }
}